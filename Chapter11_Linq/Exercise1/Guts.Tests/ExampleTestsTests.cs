﻿using Guts.Client.Shared;
using LinqExamples.Tests;
using NUnit.Framework;
using System;
using System.Linq;
using System.Reflection;
using Guts.Client.Classic;
using Guts.Client.Shared.TestTools;

namespace Guts.Tests
{
    [ExerciseTestFixture("dotnet2", 11, "1", 
        @"LinqExamples\SelectExamples.cs;LinqExamples\WhereExamples.cs;LinqExamples\OrderByExamples.cs;LinqExamples\GroupExamples.cs;LinqExamples\JoinExamples.cs;")]
    public class ExampleTestsTests
    {
        [MonitoredTest("Should not have changed test files"), Order(1)]
        public void _01_ShouldNotHaveChangedTestFiles()
        {
            var content = Solution.Current.GetFileContent(@"LinqExamples.Tests\1_SelectExamplesTests.cs").Trim();
            Assert.That(content.Length, Is.EqualTo(1650).Within(10), () => "'1_SelectExamplesTests.cs' has been changed.");

            content = Solution.Current.GetFileContent(@"LinqExamples.Tests\2_WhereExamplesTests.cs").Trim();
            Assert.That(content.Length, Is.EqualTo(1465).Within(10), () => "'2_WhereExamplesTests.cs' has been changed.");

            content = Solution.Current.GetFileContent(@"LinqExamples.Tests\3_OrderByExamplesTests.cs").Trim();
            Assert.That(content.Length, Is.EqualTo(1825).Within(10), () => "'3_OrderByExamplesTests.cs' has been changed.");

            content = Solution.Current.GetFileContent(@"LinqExamples.Tests\4_GroupExamplesTests.cs").Trim();
            Assert.That(content.Length, Is.EqualTo(2310).Within(10), () => "'4_GroupExamplesTests.cs' has been changed.");

            content = Solution.Current.GetFileContent(@"LinqExamples.Tests\5_JoinExamplesTests.cs").Trim();
            Assert.That(content.Length, Is.EqualTo(1925).Within(10), () => "'5_JoindExamplesTests.cs' has been changed.");
        }

        [MonitoredTest("Should use LINQ"), Order(2)]
        public void _02_ShouldUseLinq()
        {
            AssertUsesLinq("SelectExamples.cs");
            AssertUsesLinq("WhereExamples.cs");
            AssertUsesLinq("OrderByExamples.cs");
            AssertUsesLinq("GroupExamples.cs");
            AssertUsesLinq("JoinExamples.cs");
        }

        [MonitoredTest("All select example tests should pass"), Order(3)]
        public void _03_SelectExampleTests_ShouldAllPass()
        {
            var testClassInstance = new SelectExamplesTests();
            AssertAllTestMethodsPass(testClassInstance);
        }

        [MonitoredTest("All where example tests should pass"), Order(4)]
        public void _04_WhereExampleTests_ShouldAllPass()
        {
            var testClassInstance = new WhereExamplesTests();
            AssertAllTestMethodsPass(testClassInstance);
        }

        [MonitoredTest("All orderby example tests should pass"), Order(5)]
        public void _05_OrderByExampleTests_ShouldAllPass()
        {
            var testClassInstance = new OrderByExamplesTests();
            AssertAllTestMethodsPass(testClassInstance);
        }

        [MonitoredTest("All group example tests should pass"), Order(6)]
        public void _06_GroupExampleTests_ShouldAllPass()
        {
            var testClassInstance = new GroupExamplesTests();
            AssertAllTestMethodsPass(testClassInstance);
        }

        [MonitoredTest("All join example tests should pass"), Order(7)]
        public void _07_JoinExampleTests_ShouldAllPass()
        {
            var testClassInstance = new JoinExamplesTests();
            AssertAllTestMethodsPass(testClassInstance);
        }

        private static void AssertUsesLinq(string sourceFileName)
        {
            var content = Solution.Current.GetFileContent($@"LinqExamples\{sourceFileName}").Trim();
            content = CodeCleaner.StripComments(content).ToLower();

            Assert.That(content, Does.Not.Contain("for(").And.Not.Contain("for ("), () => $"A for-loop is used in '{sourceFileName}'. " +
                                                                 "This is not necessary when LINQ is used.");

            Assert.That(content, Does.Not.Contain("foreach(").And.Not.Contain("foreach ("), () => $"A foreach-loop is used in '{sourceFileName}'. " +
                                                                     "This is not necessary when LINQ is used.");

            Assert.That(content, Does.Not.Contain("while(").And.Not.Contain("while ("), () => $"A while-loop is used in '{sourceFileName}'. " +
                                                                   "This is not necessary when LINQ is used.");
        }

        private void AssertAllTestMethodsPass(Object testClassInstance)
        {
            var testClassType = testClassInstance.GetType();

            var setupMethod = testClassType.GetMethods()
                .FirstOrDefault(m => m.GetCustomAttribute<SetUpAttribute>() != null);

            var testMethodInfos = testClassType.GetMethods().Where(m => m.GetCustomAttribute<TestAttribute>() != null)
                .ToList();

            foreach (var testMethodInfo in testMethodInfos)
            {
                if (setupMethod != null)
                {
                    setupMethod.Invoke(testClassInstance, new object[0]);
                }
                AssertTestMethodPasses(testClassInstance, testMethodInfo);
            }
        }

        private void AssertTestMethodPasses(object testClassInstance, MethodInfo testMethod)
        {
            Assert.That(() => testMethod.Invoke(testClassInstance, new object[0]), Throws.Nothing,
                () => $"{testMethod.Name}() should pass, but doesn't.");
        }
    }
}
